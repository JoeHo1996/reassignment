import java.util.Date;
/**
 * Created by JoeHo on 17/8/17.
 */
public class ModelController {




    public Goal createGoal(User user, String name, String id, String type, Date deadline) {
        Goal goal = new Goal();
        goal.name = name;
        goal.id = id;
        goal.type = type;
        goal.deadline = deadline;
        return goal;
    }


    public User login(String username, String password) {
        return FileStorageController.checkUser(new User(username, password));
    }

    public void displayProfile(User user) {

    }

}
