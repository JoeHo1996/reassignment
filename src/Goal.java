import java.util.Date;
import java.util.UUID;

/**
 * Created by JoeHo on 17/8/17.
 */

public class Goal {
    public String name;
    public String id;
    public String type;  //default is weight
    public Date deadline;
    public boolean achieved;
    public int weight;

    public Goal(){

    }

    public Goal(String name, int weight, Date date) {
        id = UUID.randomUUID().toString();
        type = "weight";
        this.name = name;
        this.weight = weight;
        this.deadline = date;
    }
}
