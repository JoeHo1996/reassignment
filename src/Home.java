import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by JoeHo on 17/8/17.
 */
public class Home {

    User mCurrentUser;
    private JButton exitButton;
    private JButton createNewGoalButton;
    private JButton updateWeightButton;
    private JLabel healthDataL;
    private JLabel currentGoalL;
    private JLabel HistoryWeightL;
    private JLabel HistoryGoalL;
    private JPanel homePanel;
    private JLabel achievedL;
    private JLabel currentUsernameLabel;
    private static JFrame frame;


    public Home(User user) {
        mCurrentUser = user;
        initFrame();
        dispatchEvent();
        showInfo();
    }

    private void initFrame() {
        frame = new JFrame("Home");
        frame.setContentPane(homePanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 300);
        frame.setVisible(true);
    }

    private void dispatchEvent() {
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileStorageController.StoreUser(mCurrentUser);
                frame.dispose();
            }
        });

        createNewGoalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                new CreateGoalUI().setListener(new CreateGoalUI.OnCreateGoalFinishListener() {
                    @Override
                    public void onCreateGoalFinish(Goal goal) {
                        FileStorageController.updateGoalHistory(mCurrentUser, goal);
                        showInfo();
                    }
                });
            }
        });

        updateWeightButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UpdateWeightUI().setListener(new UpdateWeightUI.OnUpdateWeightFinishListener() {
                    @Override
                    public void onUpdateWeightFinish(int weight) {
                        FileStorageController.updateWeightHistory(mCurrentUser, weight);
                        showInfo();
                    }
                });
            }
        });
    }

    private void showInfo() {

        currentUsernameLabel.setText("Welcome " + mCurrentUser.name);

        String healthData = mCurrentUser.getHealthData();
        healthDataL.setText(healthData);

        String currentGoal = mCurrentUser.getCurrentGoal();
        currentGoalL.setText(currentGoal);

        String historyWeight = mCurrentUser.getHistoryWeight();
        HistoryWeightL.setText(historyWeight);

        String historyGoal = mCurrentUser.getHistoryGoal();
        HistoryGoalL.setText(historyGoal);

        String isAchieveGoal = mCurrentUser.getIfAchieveGoal();
        achievedL.setText(isAchieveGoal);
    }
}
