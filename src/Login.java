
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by JoeHo on 17/8/17.
 */

public class Login {

    private JTextField usernameTextField;
    private JTextField passwordTextField;
    private JButton LoginButton;
    private JPanel LoginPanel;
    private JButton registerAccountButton;
    private static JFrame frame;

    public Login() {

        frame = new JFrame("Login");
        frame.setContentPane(LoginPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 200);
        frame.setVisible(true);

        LoginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (usernameTextField.getText() == null || usernameTextField.getText().length() <= 0) {
                    JOptionPane.showMessageDialog(frame, "Please input username", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                if (passwordTextField.getText() == null || passwordTextField.getText().length() <= 0) {
                    JOptionPane.showMessageDialog(frame, "Please input password", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                String username = usernameTextField.getText().trim();
                String password = passwordTextField.getText().trim();

                User loginUser = FileStorageController.checkUser(new User(username, password));
                if (loginUser == null) {
                    JOptionPane.showMessageDialog(frame, "User not exist", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                frame.dispose();
                //login success
                new Home(loginUser);
            }
        });

        usernameTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    passwordTextField.requestFocus();

                }
                super.keyPressed(e);
            }
        });
        passwordTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    LoginButton.doClick();

                }
                super.keyPressed(e);
            }
        });


        registerAccountButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RegisterUI();
                frame.dispose();
            }
        });
    }


}
