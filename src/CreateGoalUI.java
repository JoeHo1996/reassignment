import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by JoeHo on 17/8/17.
 */
public class CreateGoalUI {
    private JPanel createGoalPanel;
    private JTextField goalWeightTF;
    private JButton finishBt;
    private JTextField goalNameTf;
    private JTextField mGoalDatetf;
    private static JFrame frame;
    private OnCreateGoalFinishListener listener;

    public CreateGoalUI() {
        initFrame();

        finishBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String goalWeight = goalWeightTF.getText().trim();
                int weight = 0;
                try {
                    weight = Integer.parseInt(goalWeight);

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Please input correct weight", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                String goalName = goalNameTf.getText().trim();
                if (goalName.length() == 0) {
                    JOptionPane.showMessageDialog(frame, "Please input Goal Name", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                Date goalDate;
                try {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    goalDate = format.parse(mGoalDatetf.getText().toString());
                } catch (Exception exx) {
                    JOptionPane.showMessageDialog(frame, "Please input format date", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                if (listener != null) {
                    listener.onCreateGoalFinish(new Goal(goalName, weight, goalDate));
                }

                frame.dispose();
            }
        });

    }

    private void initFrame() {
        frame = new JFrame("Goal");
        frame.setContentPane(createGoalPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setVisible(true);
    }

    interface OnCreateGoalFinishListener {
        void onCreateGoalFinish(Goal goal);
    }

    public void setListener(OnCreateGoalFinishListener listener) {
        this.listener = listener;
    }
}
