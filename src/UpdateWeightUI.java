import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by JoeHo on 17/8/17.
 */
public class UpdateWeightUI {
    private JPanel updateWeightPanel;
    private JTextField weightTf;
    private static JFrame frame;
    private JButton finishBt;
    private OnUpdateWeightFinishListener listener;

    public UpdateWeightUI() {
        initFrame();

        finishBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String goalWeight = weightTf.getText().trim();
                int weight = 0;
                try {
                    weight = Integer.parseInt(goalWeight);

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Please input correct weight", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                if (listener != null) {
                    listener.onUpdateWeightFinish(weight);
                }

                frame.dispose();
            }
        });

    }

    private void initFrame() {
        frame = new JFrame("Update Weiht");
        frame.setContentPane(updateWeightPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setVisible(true);
    }

    interface OnUpdateWeightFinishListener {
        void onUpdateWeightFinish(int weight);
    }

    public void setListener(OnUpdateWeightFinishListener listener) {
        this.listener = listener;
    }
}
