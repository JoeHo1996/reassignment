import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by JoeHo on 17/8/17.
 */

public class User {
    public String name;
    public String id;
    public String age;
    public int weight = 0;
    public String password;
    public String height;
    public String realname;
    public String email;
    public Goal goal;

    public List<Integer> weightHistory;
    public List<Goal> goals;

    public User(String username, String password) {
        this.name = username;
        this.password = password;
    }

    public User() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        return password != null ? password.equals(user.password) : user.password == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    public String getHealthData() {
        return "Weight : " + weight + "kg  " + "Age : " + age + "  Height: " + height;
    }

    public String getCurrentGoal() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        if (goal == null) {
            return "No Goal at present!";
        }
        return "You current goal weight is :" + goal.weight + "kg" + " Goal Date is " + format.format(goal.deadline);
    }

    public String getHistoryWeight() {
        if (weightHistory == null || weightHistory.isEmpty()) {
            return "No History Weight";
        }
        String str = "History Weight :";

        for (int i = 0; i < weightHistory.size() - 1; i++) {
            str += weightHistory.get(i) + "kg-> ";
        }

        str += weightHistory.get(weightHistory.size() - 1) + "kg";

        return str;
    }


    public String getHistoryGoal() {
        if (goals == null || goals.isEmpty()) {
            return "No History Weight Goal";
        }

        Collections.sort(goals, new DateComparator());

        String str = "History Weight Goal: ";

        for (int i = 0; i < goals.size() - 1; i++) {
            str += goals.get(i).weight + "kg-> ";
        }

        str += goals.get(goals.size() - 1).weight + "kg";

        return str;
    }

    public String getIfAchieveGoal() {
        if (goal.weight < weight) {
            return "You haven't achieve your weight goal! Fighting";
        } else {
            return "Congratulation! you have achieve your goal! Make a new Goal!";
        }
    }


    class DateComparator implements Comparator<Goal> {

        @Override
        public int compare(Goal g1, Goal g2) {
            int c = g1.deadline.before(g2.deadline) ? 1 : -1;
            return c;
        }
    }

}
