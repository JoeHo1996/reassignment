import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by JoeHo on 17/8/17.
 */
public class RegisterUI {
    private JTextField usernameTv;
    private JTextField passwordTv;
    private JButton registerButton;
    private JPanel registerPanel;
    private JTextField realnameTF;
    private JTextField emailTf;
    private JTextField weightTF;
    private JTextField ageTF;
    private JTextField heightTF;
    private static JFrame frame;


    public RegisterUI() {

        //set size
        frame = new JFrame("register");
        frame.setContentPane(registerPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 600);
        frame.setVisible(true);

        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String password = passwordTv.getText().trim();
                String username = usernameTv.getText().trim();
                String realname = realnameTF.getText().trim();
                String email = emailTf.getText().trim();
                String weight = weightTF.getText().trim();
                String age = ageTF.getText().trim();
                String height = heightTF.getText().trim();


                if (isEmpty(password) || isEmpty(username) || isEmpty(realname) || isEmpty(email) || isEmpty(weight) || isEmpty(age) || isEmpty(height)) {
                    JOptionPane.showMessageDialog(frame, "Please input all info", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                int weightInt = 0;
                try {
                    weightInt = Integer.parseInt(weight);

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Please input correct weight", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }

                if (FileStorageController.checkUsernameExist(username)) {
                    JOptionPane.showMessageDialog(frame, "Username has exist!", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }


                if (!FileStorageController.checkEmailFormat(email)) {
                    JOptionPane.showMessageDialog(frame, "email format error!", "Tip", JOptionPane.WARNING_MESSAGE);
                    return;
                }


                User user = new User();
                user.name = username;
                user.password = password;
                user.realname = realname;
                user.email = email;
                user.weight = weightInt;
                user.age = age;
                user.height = height;

                FileStorageController.StoreUser(user);


                JOptionPane.showMessageDialog(frame, "Register Success", "Tip", JOptionPane.WARNING_MESSAGE);
                frame.dispose();
                new Login();
            }
        });
    }


    public boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

}
