import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by JoeHo on 17/8/17.
 */
public class FileStorageController {

    //store all user by set collection
    private static final String USER_FILE_NAME = "./user.txt";
    private static Gson gson = new Gson();


    //store user to local file by json format
    public static void StoreUser(User user) {
        Set<User> userSet = getUserList();
        if (userSet == null) {
            userSet = new HashSet<>();
        }

        userSet.remove(user);
        userSet.add(user);
        writeStringToFile(gson.toJson(userSet), new File(USER_FILE_NAME));
    }

    //store user's goal to local file by json format
    public static void StoreGoal(User user, Goal goal) {
        if (user.goals == null) {
            user.goals = new ArrayList<>();
        }
        user.goals.add(goal);
        StoreUser(user);
    }

    private static Set<User> getUserList() {
        File accountFile = new File(USER_FILE_NAME);
        Gson gson = new Gson();
        Set<User> accounts = gson.fromJson(getStringFromFile(accountFile), new TypeToken<Set<User>>() {
        }.getType());
        return accounts;
    }

    public static User checkUser(User u) {
        Set<User> userList = getUserList();
        if (userList.contains(u)) {
            Iterator<User> iterator = userList.iterator();

            while (iterator.hasNext()) {
                User user = iterator.next();
                if (user.equals(u)) {
                    return user;
                }
            }
        }
        return null;
    }


    public static boolean checkUsernameExist(String name) {
        Set<User> userList = getUserList();

        if (userList == null) {
            return false;
        }

        for (User u : userList) {
            if (u.name.equals(name)) {
                return true;
            }
        }

        return false;
    }


    //file operate
    private static void writeStringToFile(String jsonStr, File file) {
        try {

            if (!file.exists()) {
                file.createNewFile();
            }

            OutputStream out = new FileOutputStream(file);
            ByteArrayInputStream in = new ByteArrayInputStream(jsonStr.getBytes());

            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                out.write(buffer, 0, len);
            }

            in.close();
            out.close();
        } catch (Exception e) {
        }
    }

    public static String getStringFromFile(File file) {
        try {
            StringBuffer sb = new StringBuffer();
            readToBuffer(sb, file);
            return sb.toString();
        } catch (Exception e) {
        }
        return null;
    }

    public static void readToBuffer(StringBuffer buffer, File file) throws IOException {
        InputStream is = new FileInputStream(file);
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        line = reader.readLine();
        while (line != null) {
            buffer.append(line);
            buffer.append("\n");
            line = reader.readLine();
        }
        reader.close();
        is.close();
    }


    public static boolean checkEmailFormat(String email) {
        String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
        Pattern p = Pattern.compile(str);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void updateGoalHistory(User user, Goal goal) {
        if (user.goal == null) {
            user.goal = goal;
        } else {

            if (user.goals == null) {
                user.goals = new ArrayList<>();
            }

            user.goals.add(user.goal);
            user.goal = goal;
        }
    }

    public static void updateWeightHistory(User mCurrentUser, int weight) {
        if (mCurrentUser.weightHistory == null) {
            mCurrentUser.weightHistory = new ArrayList<>();
        }

        mCurrentUser.weightHistory.add(mCurrentUser.weight);
        mCurrentUser.weight = weight;
    }
}
